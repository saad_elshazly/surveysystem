﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SurveySystem.Model.Entities
{
   public class TblTest:AuditableEntity<long>
    {
        public string Name { get; set; }
        public String Address { get; set; }
    }
}
