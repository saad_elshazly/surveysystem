﻿using SurveySystem.Model.Entities;
using SurveySystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveySystem.Model
{
    public class QuestionCategory : AuditableEntity<long>
    {
        public string Name { get; set; }
        public virtual ICollection<Question> Questions{get;set;}
    }
}
