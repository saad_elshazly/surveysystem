﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SurveySystem.Model;
namespace SurveySystem.Model
{
   public class Question : AuditableEntity<long>
    {
        public string  Name{ get; set; }
        public DateTime?  StartDate{ get; set; }
        public DateTime?  EndDate{ get; set; }
        public long? QuestionCategoryId { get; set; }
        [ForeignKey("QuestionCategoryId")]
        public virtual QuestionCategory QuestionCategory { get; set; }
    }
}
