namespace SurverySystem.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editQuestions : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Questions", new[] { "QuestionCategory_Id" });
            DropColumn("dbo.Questions", "QuestionCategoryId");
            RenameColumn(table: "dbo.Questions", name: "QuestionCategory_Id", newName: "QuestionCategoryId");
            AlterColumn("dbo.Questions", "QuestionCategoryId", c => c.Long());
            CreateIndex("dbo.Questions", "QuestionCategoryId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Questions", new[] { "QuestionCategoryId" });
            AlterColumn("dbo.Questions", "QuestionCategoryId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Questions", name: "QuestionCategoryId", newName: "QuestionCategory_Id");
            AddColumn("dbo.Questions", "QuestionCategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Questions", "QuestionCategory_Id");
        }
    }
}
