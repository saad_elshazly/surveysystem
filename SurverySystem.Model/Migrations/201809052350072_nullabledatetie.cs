namespace SurverySystem.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullabledatetie : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Questions", "StartDate", c => c.DateTime());
            AlterColumn("dbo.Questions", "EndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Questions", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Questions", "StartDate", c => c.DateTime(nullable: false));
        }
    }
}
