namespace SurverySystem.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StartEndDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Questions", "EndDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Questions", "EndDate");
            DropColumn("dbo.Questions", "StartDate");
        }
    }
}
