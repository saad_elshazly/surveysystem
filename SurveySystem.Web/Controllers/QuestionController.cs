﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Survey.Service;
using SurveySystem.Model;
using SurveySystem.Service;


namespace SurveySystem.Web.Controllers
{
    public class QuestionController : Controller
    {
        IQuestionService _questionService;
        public QuestionController(IQuestionService QuestionService)
        {
            _questionService = QuestionService;
        }

        //get all services
        public JsonResult GetAll()
        {
            List<Question> question = null;
            try
            {
                question = _questionService.GetAll().ToList();

            }
            catch (Exception e)
            {
                return Json(new { Status = 500, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = 200, Result = question.Select(x => new { x.Id, x.Name, x.StartDate, x.EndDate, CatName = x.QuestionCategory?.Name, x.QuestionCategoryId }) }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Add(Question Model)
        {
            try
            {
                _questionService.Insert(Model);
            }
            catch (Exception e)
            {
                return Json(new { Status = 500, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = 200 }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(Question Model)
        {
            try
            {
                _questionService.Edit(Model);
            }
            catch (Exception e)
            {
                return Json(new { Status = 500, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = 200 }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Remove(int Id)
        {
            try
            {
                _questionService.Remove(Id);
            }
            catch (Exception e)
            {
                return Json(new { Status = 500, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = 200 }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetById(int Id)
        {
            Question question = null;
            try
            {
                question = _questionService.GetById(Id);
            }
            catch (Exception e)
            {
                return Json(new { Status = 500, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = 200, Result = new { question.Id, question.Name, question.StartDate, question.EndDate, question.QuestionCategoryId } }, JsonRequestBehavior.AllowGet);
        }






    }
}
