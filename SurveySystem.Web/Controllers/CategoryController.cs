﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Survey.Service;
using SurveySystem.Model;
using SurveySystem.Service;


namespace SurveySystem.Web.Controllers
{
    public class CategoryController : Controller
    {
        ICategoryService _catService;
        public CategoryController(ICategoryService cateService)
        {
            _catService = cateService;
        }

        //get all services
        public JsonResult GetAll()
        {
            List<QuestionCategory> cat = null;
            try
            {
                cat = _catService.GetAll().ToList();

            }
            catch (Exception e)
            {
                return Json(new { Status = HttpStatusCode.InternalServerError,Message=e.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = HttpStatusCode.OK, Result = cat.Select(x=>new { x.Name,x.Id }) }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Add(QuestionCategory Model)
        {
            try
            {
                _catService.Insert(Model);
            }
            catch (Exception e)
            {
                return Json(new { Status = HttpStatusCode.InternalServerError, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = HttpStatusCode.OK }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(QuestionCategory Model)
        {
            try
            {
                _catService.Edit(Model);
            }
            catch (Exception e)
            {
                return Json(new { Status = HttpStatusCode.InternalServerError, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = HttpStatusCode.OK }, JsonRequestBehavior.AllowGet);
        }
       
        public JsonResult Delete(int Id)
        {
            try
            {
                _catService.Delete(Id);
            }
            catch (Exception e)
            {
                return Json(new { Status = HttpStatusCode.InternalServerError, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = HttpStatusCode.OK }, JsonRequestBehavior.AllowGet);
        }




    }
}
