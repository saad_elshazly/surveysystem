﻿import { NgModule, ErrorHandler } from '@angular/core';
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule, MdNativeDateModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';
import { CategoryComponent } from "./components/category.component";
import { QuestionComponent } from "./components/question.component";
import { CategoryService } from "./services/category.service";
import { QuestionService } from "./services/Question.service";
import { JsonToDatePipe } from "./pipes/Date.pipe";
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
@NgModule({
    imports: [BrowserModule, ReactiveFormsModule,  HttpModule, routing, FormsModule,
        BrowserAnimationsModule,
        MaterialModule, Ng2Bs3ModalModule,
        MdNativeDateModule],
    declarations: [AppComponent, CategoryComponent, QuestionComponent, JsonToDatePipe],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy }, CategoryService,QuestionService],
    bootstrap: [AppComponent],
    //entryComponents: [StackOverflowComponent]

})
export class AppModule { }
