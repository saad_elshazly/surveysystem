"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var platform_browser_1 = require("@angular/platform-browser");
var animations_1 = require("@angular/platform-browser/animations");
var material_1 = require("@angular/material");
var forms_1 = require("@angular/forms");
var app_component_1 = require("./app.component");
var http_1 = require("@angular/http");
var app_routing_1 = require("./app.routing");
var category_component_1 = require("./components/category.component");
var question_component_1 = require("./components/question.component");
var category_service_1 = require("./services/category.service");
var Question_service_1 = require("./services/Question.service");
var Date_pipe_1 = require("./pipes/Date.pipe");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.ReactiveFormsModule, http_1.HttpModule, app_routing_1.routing, forms_1.FormsModule,
                animations_1.BrowserAnimationsModule,
                material_1.MaterialModule, ng2_bs3_modal_1.Ng2Bs3ModalModule,
                material_1.MdNativeDateModule],
            declarations: [app_component_1.AppComponent, category_component_1.CategoryComponent, question_component_1.QuestionComponent, Date_pipe_1.JsonToDatePipe],
            providers: [
                { provide: common_1.LocationStrategy, useClass: common_1.HashLocationStrategy }, category_service_1.CategoryService, Question_service_1.QuestionService
            ],
            bootstrap: [app_component_1.AppComponent],
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map