﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class CategoryService {
    constructor(private _http: Http) { }
    get(url: string): Observable<any> {
      
        return this._http.get(url)
            .map((response: Response) => <any>response.json());
    }
    add(url: string, model: any): Observable<any> {

        var json = JSON.stringify({ Model: model });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this._http.post(url, json, options)
            .map((response: Response) => <any>response.json());
    }
    update(url: string, model: any): Observable<any> {

        var json = JSON.stringify({ Model: model });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this._http.post(url, json, options)
            .map((response: Response) => <any>response.json());
    }
    remove(url: string, id: number): Observable<any> {
       
        var json = JSON.stringify({ Id: id });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this._http.post(url, json, options)
            .map((response: Response) => <any>response.json());
    }
    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}