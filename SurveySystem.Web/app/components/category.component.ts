﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from "../services/category.service";
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
    templateUrl: "/app/views/category.component.html"
})
export class CategoryComponent implements OnInit {

    Title: string = "Category Page";
    data: any;
    currentId: number = 0;
    isAdd: boolean;
    form: FormGroup;
    @ViewChild('DeleteModal') DeleteModal: ModalComponent;
    @ViewChild('AddUpdateModal') AddUpdateModal: ModalComponent;
    model = {
        Id: 0,
        Name:''
    }
    constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private service: CategoryService) {

    }
    ngOnInit(): void {
        this.getAll();
        this.form = this.formBuilder.group({
            Name: [null, Validators.required],
        });
    }
    getAll() {
        this.service.get("Category/GetAll").subscribe(response => {
            this.data = response.Result;
            console.log(this.data);
        }, error => {
            console.log(error.Message);
        });
    }
    openDeleteModal(id: number) {
        debugger;
        this.currentId = id;
        this.DeleteModal.open();
    }
    delete() {
        console.log(this.currentId);
        if (this.currentId != 0) {
            this.service.remove("Category/Delete", this.currentId).subscribe(res => {
                this.currentId = 0;
                console.log("data deleted successfully");
                this.getAll();
                this.DeleteModal.close();
            }, error => {
                console.log(error);
            });
        }

    }
    newCat() {
        this.isAdd = true
        this.model.Name = '';
        this.AddUpdateModal.open();
    }
    openEditModal(cat: any) {
        this.model = cat;
        this.isAdd = false;
        this.AddUpdateModal.open();
    }
    AddUpdateSubmit() {
        if (this.isAdd) {
            this.service.add("Category/Add", this.model).subscribe(res => {
                console.log("data added");
                this.getAll();
                this.AddUpdateModal.close();
            }, Error => {
                });
        }
        else
        {
            this.service.add("Category/Update", this.model).subscribe(res => {
                console.log("data Updated");
                this.getAll();
                this.AddUpdateModal.close();
            }, Error => {
            });
        }
    }
}