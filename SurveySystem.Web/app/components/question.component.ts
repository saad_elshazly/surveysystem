﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionService } from "../services/Question.service";
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from "../services/category.service";
import { DatePipe } from '@angular/common';
import { JsonToDatePipe } from "../pipes/Date.pipe";
@Component({
    templateUrl: "/app/views/question.component.html",
    providers: [JsonToDatePipe]
})
export class QuestionComponent implements OnInit {

    Title: string = "Question";
    data: any;
    currentId: number = 0;
    isAdd: boolean;

    cats: any;
    form: FormGroup;
    @ViewChild('DeleteModal') DeleteModal: ModalComponent;
    @ViewChild('AddUpdateModal') AddUpdateModal: ModalComponent;
    model = {
        Id: 0,
        Name: '',
        StartDate: '',
        EndDate: '',
        QuestionCategoryId: 0,
        QuestionCategoryName: '',
    }
    constructor(private jsonToDatePipe: JsonToDatePipe,private route: ActivatedRoute, private formBuilder: FormBuilder, private service: QuestionService, private catService: CategoryService) {

    }
    ngOnInit(): void {
        this.loadAll();
        this.form = this.formBuilder.group({
            Name: [null, Validators.required],
            StartDate: [null, Validators.required],
            EndDate: [null, Validators.required],
            category: []
        });
    }
    loadAll() {
        this.service.get("Question/GetAll").subscribe(res => {
            this.data = res.Result;
            console.log(this.data);
        }, error => {
            console.log(error.Message);
        });
    }
    loadCategories() {
        this.service.get("Category/GetAll").subscribe(res => {
            this.cats = res.Result;
        }, error => { });
    }
    openDeleteModal(id: number) {
        debugger;
        this.currentId = id;
        this.DeleteModal.open();
    }
    delete() {
        console.log(this.currentId);
        if (this.currentId != 0) {
            this.service.remove("Question/Remove", this.currentId).subscribe(res => {
                this.currentId = 0;
                console.log("data deleted successfully");
                this.loadAll();
                this.DeleteModal.close();
            }, error => {
                console.log(error);
            });
        }

    }
    getById(id:number):any {
        var quest;
        this.service.getById("Question/GetById", id).subscribe(res => {
            quest = res.Result;
        }, error => {
            });
        return quest;
    }
    newCat() {
        this.isAdd = true
        this.model.Name = '';
        this.loadCategories();
        this.model.QuestionCategoryId = 0;
        this.AddUpdateModal.open();
    }
    openEditModal(quest: any) {
        //var question = this.getById(quest.Id);
        //debugger;
        quest.StartDate = this.jsonToDatePipe.transform(quest.StartDate);
        quest.EndDate = this.jsonToDatePipe.transform(quest.EndDate);
        this.model = quest;
        this.loadCategories();
        if (quest.QuestionCategoryId == null) {
            this.model.QuestionCategoryId = 0;
        }
        else
            this.model.QuestionCategoryId = quest.QuestionCategoryId;
        this.isAdd = false;
        this.AddUpdateModal.open();
    }
    AddUpdateSubmit() {
        if (this.isAdd) {
            this.service.add("Question/Add", this.model).subscribe(res => {
                console.log("data added");
                this.loadAll();
                this.AddUpdateModal.close();
            }, Error => {
            });
        }
        else {
            this.service.update("Question/Update", this.model).subscribe(res => {
                console.log("data Updated");
                this.loadAll();
                this.AddUpdateModal.close();
            }, Error => {
            });
        }
    }
    onStartDateChange(date: any) {
        debugger;
        this.model.StartDate = date.currentTarget.value;
    }
    onEndDateChange(date: any) {
        debugger;
        this.model.EndDate = date.currentTarget.value;
    }
    //dateConvert(date: string) {
    //    var datePipe = new DatePipe("en-US");
    //    date = datePipe.transform(new Date(parseInt(date.replace('/Date(', ''))), 'MM/dd/yyyy');
    //    return date;
    //}

}