"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var category_service_1 = require("../services/category.service");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var forms_1 = require("@angular/forms");
var CategoryComponent = (function () {
    function CategoryComponent(route, formBuilder, service) {
        this.route = route;
        this.formBuilder = formBuilder;
        this.service = service;
        this.Title = "Category Page";
        this.currentId = 0;
        this.model = {
            Id: 0,
            Name: ''
        };
    }
    CategoryComponent.prototype.ngOnInit = function () {
        this.getAll();
        this.form = this.formBuilder.group({
            Name: [null, forms_1.Validators.required],
        });
    };
    CategoryComponent.prototype.getAll = function () {
        var _this = this;
        this.service.get("Category/GetAll").subscribe(function (response) {
            _this.data = response.Result;
            console.log(_this.data);
        }, function (error) {
            console.log(error.Message);
        });
    };
    CategoryComponent.prototype.openDeleteModal = function (id) {
        debugger;
        this.currentId = id;
        this.DeleteModal.open();
    };
    CategoryComponent.prototype.delete = function () {
        var _this = this;
        console.log(this.currentId);
        if (this.currentId != 0) {
            this.service.remove("Category/Delete", this.currentId).subscribe(function (res) {
                _this.currentId = 0;
                console.log("data deleted successfully");
                _this.getAll();
                _this.DeleteModal.close();
            }, function (error) {
                console.log(error);
            });
        }
    };
    CategoryComponent.prototype.newCat = function () {
        this.isAdd = true;
        this.model.Name = '';
        this.AddUpdateModal.open();
    };
    CategoryComponent.prototype.openEditModal = function (cat) {
        this.model = cat;
        this.isAdd = false;
        this.AddUpdateModal.open();
    };
    CategoryComponent.prototype.AddUpdateSubmit = function () {
        var _this = this;
        if (this.isAdd) {
            this.service.add("Category/Add", this.model).subscribe(function (res) {
                console.log("data added");
                _this.getAll();
                _this.AddUpdateModal.close();
            }, function (Error) {
            });
        }
        else {
            this.service.add("Category/Update", this.model).subscribe(function (res) {
                console.log("data Updated");
                _this.getAll();
                _this.AddUpdateModal.close();
            }, function (Error) {
            });
        }
    };
    __decorate([
        core_1.ViewChild('DeleteModal'),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], CategoryComponent.prototype, "DeleteModal", void 0);
    __decorate([
        core_1.ViewChild('AddUpdateModal'),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], CategoryComponent.prototype, "AddUpdateModal", void 0);
    CategoryComponent = __decorate([
        core_1.Component({
            templateUrl: "/app/views/category.component.html"
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute, forms_1.FormBuilder, category_service_1.CategoryService])
    ], CategoryComponent);
    return CategoryComponent;
}());
exports.CategoryComponent = CategoryComponent;
//# sourceMappingURL=category.component.js.map