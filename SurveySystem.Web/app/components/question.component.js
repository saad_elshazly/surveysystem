"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Question_service_1 = require("../services/Question.service");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var forms_1 = require("@angular/forms");
var category_service_1 = require("../services/category.service");
var Date_pipe_1 = require("../pipes/Date.pipe");
var QuestionComponent = (function () {
    function QuestionComponent(jsonToDatePipe, route, formBuilder, service, catService) {
        this.jsonToDatePipe = jsonToDatePipe;
        this.route = route;
        this.formBuilder = formBuilder;
        this.service = service;
        this.catService = catService;
        this.Title = "Question";
        this.currentId = 0;
        this.model = {
            Id: 0,
            Name: '',
            StartDate: '',
            EndDate: '',
            QuestionCategoryId: 0,
            QuestionCategoryName: '',
        };
    }
    QuestionComponent.prototype.ngOnInit = function () {
        this.loadAll();
        this.form = this.formBuilder.group({
            Name: [null, forms_1.Validators.required],
            StartDate: [null, forms_1.Validators.required],
            EndDate: [null, forms_1.Validators.required],
            category: []
        });
    };
    QuestionComponent.prototype.loadAll = function () {
        var _this = this;
        this.service.get("Question/GetAll").subscribe(function (res) {
            _this.data = res.Result;
            console.log(_this.data);
        }, function (error) {
            console.log(error.Message);
        });
    };
    QuestionComponent.prototype.loadCategories = function () {
        var _this = this;
        this.service.get("Category/GetAll").subscribe(function (res) {
            _this.cats = res.Result;
        }, function (error) { });
    };
    QuestionComponent.prototype.openDeleteModal = function (id) {
        debugger;
        this.currentId = id;
        this.DeleteModal.open();
    };
    QuestionComponent.prototype.delete = function () {
        var _this = this;
        console.log(this.currentId);
        if (this.currentId != 0) {
            this.service.remove("Question/Remove", this.currentId).subscribe(function (res) {
                _this.currentId = 0;
                console.log("data deleted successfully");
                _this.loadAll();
                _this.DeleteModal.close();
            }, function (error) {
                console.log(error);
            });
        }
    };
    QuestionComponent.prototype.getById = function (id) {
        var quest;
        this.service.getById("Question/GetById", id).subscribe(function (res) {
            quest = res.Result;
        }, function (error) {
        });
        return quest;
    };
    QuestionComponent.prototype.newCat = function () {
        this.isAdd = true;
        this.model.Name = '';
        this.loadCategories();
        this.model.QuestionCategoryId = 0;
        this.AddUpdateModal.open();
    };
    QuestionComponent.prototype.openEditModal = function (quest) {
        //var question = this.getById(quest.Id);
        //debugger;
        quest.StartDate = this.jsonToDatePipe.transform(quest.StartDate);
        quest.EndDate = this.jsonToDatePipe.transform(quest.EndDate);
        this.model = quest;
        this.loadCategories();
        if (quest.QuestionCategoryId == null) {
            this.model.QuestionCategoryId = 0;
        }
        else
            this.model.QuestionCategoryId = quest.QuestionCategoryId;
        this.isAdd = false;
        this.AddUpdateModal.open();
    };
    QuestionComponent.prototype.AddUpdateSubmit = function () {
        var _this = this;
        if (this.isAdd) {
            this.service.add("Question/Add", this.model).subscribe(function (res) {
                console.log("data added");
                _this.loadAll();
                _this.AddUpdateModal.close();
            }, function (Error) {
            });
        }
        else {
            this.service.update("Question/Update", this.model).subscribe(function (res) {
                console.log("data Updated");
                _this.loadAll();
                _this.AddUpdateModal.close();
            }, function (Error) {
            });
        }
    };
    QuestionComponent.prototype.onStartDateChange = function (date) {
        debugger;
        this.model.StartDate = date.currentTarget.value;
    };
    QuestionComponent.prototype.onEndDateChange = function (date) {
        debugger;
        this.model.EndDate = date.currentTarget.value;
    };
    __decorate([
        core_1.ViewChild('DeleteModal'),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], QuestionComponent.prototype, "DeleteModal", void 0);
    __decorate([
        core_1.ViewChild('AddUpdateModal'),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], QuestionComponent.prototype, "AddUpdateModal", void 0);
    QuestionComponent = __decorate([
        core_1.Component({
            templateUrl: "/app/views/question.component.html",
            providers: [Date_pipe_1.JsonToDatePipe]
        }),
        __metadata("design:paramtypes", [Date_pipe_1.JsonToDatePipe, router_1.ActivatedRoute, forms_1.FormBuilder, Question_service_1.QuestionService, category_service_1.CategoryService])
    ], QuestionComponent);
    return QuestionComponent;
}());
exports.QuestionComponent = QuestionComponent;
//# sourceMappingURL=question.component.js.map