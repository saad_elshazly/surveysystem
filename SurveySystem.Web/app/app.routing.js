"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var category_component_1 = require("./components/category.component");
var question_component_1 = require("./components/question.component");
var appRoutes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: category_component_1.CategoryComponent },
    { path: 'question', component: question_component_1.QuestionComponent }
    //{ path: 'user', component: UserComponent }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map