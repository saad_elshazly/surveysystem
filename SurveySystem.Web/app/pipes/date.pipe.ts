﻿import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({ name: 'jsontodate' })
export class JsonToDatePipe implements PipeTransform {
    transform(value: any): any {
        var datePipe = new DatePipe("en-US");
        value = datePipe.transform(new Date(parseInt(value.replace('/Date(', ''))), 'yyyy-MM-dd');
        return value;
    }
}