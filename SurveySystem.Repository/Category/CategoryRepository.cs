﻿
using SurveySystem.Model;
using SurveySystem.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArabicLexicon.Repository.Test
{
   public class CategoryRepository : GenericRepository<QuestionCategory>, ICategoryRepository
    {
        public CategoryRepository(DbContext context)
            : base(context)
        {

        }
    }
}
