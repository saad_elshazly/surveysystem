﻿using SurveySystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SurveySystem.Repository
{
  public interface ICategoryRepository:IGenericRepository<QuestionCategory>
    {

    }
}
