﻿
using SurveySystem.Model;
using SurveySystem.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArabicLexicon.Repository.Test
{
   public class QuestionRepository:GenericRepository<Question>,IQuestionRepository
    {
        public QuestionRepository(DbContext context)
            : base(context)
        {

        }
    }
}
