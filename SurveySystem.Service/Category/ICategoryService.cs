﻿
using SurveySystem.Model;
using SurveySystem.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Service
{
   public interface ICategoryService:IEntityService<QuestionCategory>
    {
        void Insert(QuestionCategory cat);
        void Edit(QuestionCategory cat);
        void Delete(int Id);


    }
}
