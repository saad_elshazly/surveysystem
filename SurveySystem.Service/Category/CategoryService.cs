﻿
using Survey.Service;
using SurveySystem.Model;
using SurveySystem.Repository;
using SurveySystem.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveySystem.Service
{
   public class CategoryService : EntityService<QuestionCategory>,ICategoryService
    {
       
        IUnitOfWork _unitOfWork;
        ICategoryRepository _CategoryRepository;
        public CategoryService(IUnitOfWork unitOfWork, ICategoryRepository categoryRepository)
            : base(unitOfWork, categoryRepository)
        {
            _unitOfWork = unitOfWork;
            _CategoryRepository = categoryRepository;
        }

        public void Delete(int Id)
        {
            var cat = _CategoryRepository.FindBy(x => x.Id == Id).FirstOrDefault();
            if (cat!=null)
            {
                foreach (var item in cat.Questions.ToList())
                {
                    cat.Questions.Remove(item);
                }
                _CategoryRepository.Delete(cat);
                _CategoryRepository.Save();
            }
        }

        public void Edit(QuestionCategory cat)
        {
           var _cat= _CategoryRepository.FindBy(x => x.Id == cat.Id).FirstOrDefault();
            if (_cat != null)
            {
                _cat.Name = cat.Name;
            }
            _CategoryRepository.Edit(_cat);
            _CategoryRepository.Save();
        }
        public void Insert(QuestionCategory cat)
        {
            QuestionCategory _cat = new QuestionCategory() { Name=cat.Name};
            _CategoryRepository.Add(_cat);
            _CategoryRepository.Save();
        }
       
    }
}
