﻿
using SurveySystem.Model;
using SurveySystem.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Service
{
   public interface IQuestionService:IEntityService<Question>
    {
        void Insert(Question question);
        void Edit(Question question);
        void Remove(int Id);
        Question GetById(int Id);


    }
}
