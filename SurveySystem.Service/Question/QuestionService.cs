﻿
using Survey.Service;
using SurveySystem.Model;
using SurveySystem.Repository;
using SurveySystem.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveySystem.Service
{
   public class QuestionService:EntityService<Question>,IQuestionService
    {
       
        IUnitOfWork _unitOfWork;
        IQuestionRepository _QuestionRepository;
        public QuestionService(IUnitOfWork unitOfWork, IQuestionRepository questionRepository)
            : base(unitOfWork, questionRepository)
        {
            _unitOfWork = unitOfWork;
            _QuestionRepository = questionRepository;
        }

        public void Remove(int Id)
        {
            var question = _QuestionRepository.FindBy(x => x.Id == Id).FirstOrDefault();
            if (question!=null)
            {
                _QuestionRepository.Delete(question);
                _QuestionRepository.Save();
            }
        }

        public void Edit(Question question)
        {
           var _question= _QuestionRepository.FindBy(x => x.Id == question.Id).FirstOrDefault();
            if (_question!=null)
            {
                _question.Name = question.Name;
                _question.QuestionCategoryId = question.QuestionCategoryId;
                _question.StartDate = question.StartDate;
                _question.EndDate = question.EndDate;
            }
            _QuestionRepository.Edit(_question);
            _QuestionRepository.Save();

        }
        public void Insert(Question question)
        {
            Question _question = new Question() { Name=question.Name, QuestionCategoryId= question.QuestionCategoryId, StartDate=question.StartDate,EndDate=question.EndDate  };
            _QuestionRepository.Add(_question);
            _QuestionRepository.Save();
        }

        public Question GetById(int Id)
        {
            return _QuestionRepository.FindBy(x => x.Id == Id).FirstOrDefault();
        }
    }
}
